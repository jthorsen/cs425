package kvstore

import (
	"log"
	"cs425/mp4/gossiper"
	"sync"
)

type Value struct {
	Clock *gossiper.Vclock
	Data []byte
}

type KVStore struct {
	Data map[uint32] *Value
	Lock sync.Mutex
}

func NewKVStore() *KVStore {
	return &KVStore { 
		Data: make(map[uint32] *Value),
		Lock: sync.Mutex{},
	}
}

func (kv *KVStore) Write(key uint32, value []byte, clock *gossiper.Vclock) (bool) {
	kv.Lock.Lock()
	ret := true
	curr, exists := kv.Data[key];
	if !exists || curr.Clock.LessThan(clock) {
		if exists {
			curr.Data = value
			curr.Clock = clock
			if value != nil {
				log.Printf("KVStore: UPDATE %d\n", key)
			} else {
				log.Printf("KVStore: DELETE %d\n", key)
			}
		} else {
			kv.Data[key] = &Value {
				Data: value,
				Clock: clock,
			}
			if value != nil {
				log.Printf("KVStore: INSERT %d\n", key)
			} else {
				log.Printf("KVStore: DELETE %d\n", key)
			}
		}
	} else {
		ret = false //tried to write older data
	} 
	kv.Lock.Unlock()
	return ret
}

func (kv *KVStore) Read(key uint32) (value *Value, present bool) {
	kv.Lock.Lock()
    log.Printf("KVStore: LOOKUP %d\n", key)
	value, present = kv.Data[key]
	kv.Lock.Unlock()
    return
}
