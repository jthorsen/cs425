package gossiper

import (
   "code.google.com/p/goprotobuf/proto"
   "cs425/mp4/hostid"
)

type Vclock struct {
    hosts map[hostid.HostID] uint64
}

func (v *Vclock) Compare(o *Vclock) int {
    gt, lt := 1, -1
    for host, hb := range v.hosts {
        hb2, exists := o.hosts[host]
        if exists {
            if hb < hb2 {
                gt = 0
            } else if hb > hb2 {
                lt = 0
            }
        }
    }
    return gt + lt
}

func (v *Vclock) LessThan(o *Vclock) bool {
    return v.Compare(o) < 0
}

func (v *Vclock) GreaterThan(o *Vclock) bool {
    return v.Compare(o) > 0
}

func (data *Vclock_Proto) ToVclock() *Vclock {
    vclock := &Vclock {
        hosts: make(map[hostid.HostID] uint64, len(data.GetHosts())),
    }
    for _, data := range data.GetHosts() {
        vclock.hosts[*data.GetHost().ToHostID()] = data.GetHeartbeat()
    }
    return vclock
}

func (data *Vclock) ToProto() *Vclock_Proto {
    vclock := &Vclock_Proto {
        Hosts: make([]*Vclock_Proto_Entry, 0, len(data.hosts)),
    }
    for host, hb := range data.hosts {
        vclock.Hosts = append(vclock.Hosts, &Vclock_Proto_Entry {
            Host: host.ToProto(),
            Heartbeat: proto.Uint64(hb),
        })
    }
    return vclock
}
