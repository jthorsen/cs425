package main

import (
	"log"
	"net"
	"cs425/mp4/hostid"
)

const (
	PRINTER_ID uint32 = 0
)

type printer struct {
		
}

func (p * printer) GetId() uint32 {
	return PRINTER_ID
}

func (p * printer) GetData(host hostid.HostID) []byte {
	return nil
}

func (p * printer) OnUpdate(host hostid.HostID, data []byte) {
}

func (p * printer) OnJoin(host hostid.HostID) {
	log.Printf("Gossiper: JOIN %s:%d\n", net.IP(host.IP), host.Port)
}

func (p * printer) OnShutdown(host hostid.HostID) {
	log.Printf("Gossiper: SHUTDOWN %s:%d\n", net.IP(host.IP), host.Port)
}

func (p * printer) OnStop() {
}
