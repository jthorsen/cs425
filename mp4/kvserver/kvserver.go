package kvserver

import (
	"net"
	"code.google.com/p/goprotobuf/proto"
	"cs425/mp4/kvstore"
	"cs425/mp4/hostid"
	"cs425/mp4/gossiper"
	"log"
	"math"
	"sort"
	"sync"
	"encoding/binary"
	"errors"
)

const (
	NUM_REPLICAS = 3
	NUM_KEYS = 1000000
)

type Hosts []*hostid.HostID

func (h Hosts) Len() int {
	return len(h)
}

func (h Hosts) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h Hosts) Less(i, j int) bool {
	if h[i].IP == h[j].IP {
		if h[i].Port == h[j].Port {
			return h[i].Time < h[j].Time
		}
		return h[i].Port < h[j].Port
	}
	return h[i].IP < h[j].IP
}

type KVServer struct {
	host *hostid.HostID
	addr *net.TCPAddr
	sock *net.TCPListener
	hosts Hosts
	store *kvstore.KVStore
	gossiper *gossiper.Gossiper
	lock sync.Mutex
}

func (s *KVServer) GetHostAsync(key uint32) *hostid.HostID {
	chunk := math.Ceil(float64(NUM_KEYS+1)/float64(len(s.hosts)))
	return s.hosts[key / uint32(chunk)]
}

func (s *KVServer) GetHost(key uint32) *hostid.HostID {
	s.lock.Lock()
	host := s.GetHostAsync(key)
	s.lock.Unlock()
	return host
}

func (s *KVServer) GetReplicasAsync(key uint32) Hosts {	
	end := len(s.hosts)
	chunk := math.Ceil(float64(NUM_KEYS+1)/float64(end))
	start := int(key / uint32(chunk))
	var hosts Hosts;
	for i:=0; i<NUM_REPLICAS && i < end; i++ {
		hosts = append(hosts, s.hosts[start])
		start++
		if start >= end {
			start = 0
		}
	}
	return hosts
}

func (s *KVServer) GetReplicas(key uint32) Hosts {	
	s.lock.Lock()
	hosts := s.GetReplicasAsync(key)
	s.lock.Unlock()
	return hosts
}

func (s *KVServer) Contains(key uint32) bool {
	replicas := s.GetReplicas(key)
	for _, host := range replicas {
		if *host == *s.host {
			return true
		}
	}
	return false
}

func NewKVServer(host *hostid.HostID, goss *gossiper.Gossiper) *KVServer {
	return &KVServer {
		host: host,
		store: kvstore.NewKVStore(),
		hosts: Hosts{host},
		gossiper: goss,
	}
}

func (s *KVServer) Start() error {
	s.addr = s.host.ToTCPAddr()

	var err error
	s.sock, err = net.ListenTCP("tcp", s.addr)
	if err != nil {
		return err
	}
	go s.listen()
	return nil
}

func (s *KVServer) listen() {
	for {
		conn, err := s.sock.AcceptTCP()
		if err != nil {
			log.Printf("KVServer: listen: %s\n", err.Error())
		}
		go s.handleRequest(conn)
	}
}

func (p *Pair) NilChan(channels []chan *SingleResponse) {
	for _, i := range p.Chs {
		channels[i] <- nil
	}
}

func (s *KVServer) MakeRequest(pair *Pair, channels []chan *SingleResponse, host hostid.HostID) {
	req := pair.Req

	addr := host.ToTCPAddr()
	conn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		log.Printf("KVServer: MakeRequest: %s\n", err.Error())
		pair.NilChan(channels)
		return
	}

	err = SendProto(conn, req)
	if err != nil {
		log.Printf("KVServer: MakeRequest: %s\n", err.Error())
		pair.NilChan(channels)
	}

	data, err := ReadAll(conn)
	if err != nil {
		log.Printf("KVServer: MakeRequest: %s\n", err.Error())
		pair.NilChan(channels)
		return
	}

	resps := &MultiResponse{}
	err = proto.Unmarshal(data, resps)
	if err != nil {
		log.Printf("KVServer: MakeRequest: %s\n", err.Error())
		pair.NilChan(channels)
		return
	}

	conn.Close()

	for i, resp := range resps.GetResps() {
		channels[pair.Chs[i]] <- resp
	}

}

func ReadAll(conn *net.TCPConn) ([]byte, error) {

	ret := []byte{}
	data := make([]byte, 1024)
	for len(ret) < 4 {
		count, err := conn.Read(data)
		if err != nil {
			return ret, errors.New("ReallAll: Failed to read size")
		}
		if count == 0 {
			return ret, nil
		}
		
		ret = append(ret, data[:count]...)
	}
	size := int(binary.BigEndian.Uint32(ret[:4]))
	ret = ret[4:]
	for len(ret) < size {
		count, err := conn.Read(data)
		if err != nil {
			return ret, err
		}
		if count == 0 {
			return ret, nil
		}
		
		ret = append(ret, data[:count]...)
 	}

	return ret, nil
}

func (s *KVServer) handleRequest(conn *net.TCPConn) {
	
	data, err := ReadAll(conn)
	if err != nil {
		log.Printf("KVServer: handleRequest: %s\n", err.Error())
	}	

	req := &MultiRequest{}
	err = proto.Unmarshal(data, req)
	if err != nil {
		log.Printf("KVServer: handleRequest: %s\n", err.Error())
	}

	resp := s.handleQuery(req)

	err = SendProto(conn, resp)
	if err != nil {
		log.Printf("KVServer: handleRequest: %s\n", err.Error())
	}
    conn.Close()
}

type Pair struct {
	Req *MultiRequest
	Chs []int
}

func (s *KVServer) handleQuery(reqs *MultiRequest) *MultiResponse {
	redirects := make(map[hostid.HostID] *Pair, 1)
	resp := &MultiResponse {
		Resps: make([]*SingleResponse, 0),
	}

	channels := make([]chan *SingleResponse, len(reqs.Reqs))
	for i, _ := range channels {
		channels[i] = make(chan *SingleResponse, NUM_REPLICAS)
	}
	requirements := make([]int, len(channels))

	for i, req := range reqs.Reqs {

		key := req.GetKey()
		client := req.GetClient()

		if client {
			req.Client = proto.Bool(false)
			req.Clock = s.gossiper.GetVclock().ToProto()
			consistency := req.GetConsistency()
			replicas := s.GetReplicas(key)
			if consistency == SingleRequest_One {
				requirements[i] = 1
			} else if consistency == SingleRequest_Quorum {
				requirements[i] = NUM_REPLICAS/2 + 1
			} else if consistency == SingleRequest_All {
				requirements[i] = NUM_REPLICAS
			}
			if len(replicas) < requirements[i] {
				requirements[i] = len(replicas)
			}	


			for _, replica := range replicas {
				_, exists := redirects[*replica]
				if !exists {
					redirects[*replica] = &Pair{
						Req: &MultiRequest {
							Reqs: make([]*SingleRequest, 0),
						},
						Chs: make([]int, 0),		
					}				
				}
				redirects[*replica].Req.Reqs = append(redirects[*replica].Req.Reqs, req)
				redirects[*replica].Chs = append(redirects[*replica].Chs, i)
			}

		} else {
			requirements[i] = 1
			channels[i] <- s.Execute(req)
		}	
	}


	for host, pair := range redirects {
		if host != *s.host {
			go s.MakeRequest(pair, channels, host)
		} else {
			for i, req := range pair.Req.GetReqs() {
				channels[pair.Chs[i]] <- s.Execute(req)
			}
		}
	}
	
	for i, ch := range channels {
		remaining := int(math.Min(float64(NUM_REPLICAS), float64(len(s.hosts))))
		var count = 0
		final := &SingleResponse {}
		for remaining > 0 && count < requirements[i]{
	 		resp := <- ch
			remaining--
			if resp != nil {
				count++
				if reqs.Reqs[i].GetOp() == SingleRequest_Read && (final.GetClock() == nil || final.GetClock().ToVclock().LessThan(resp.GetClock().ToVclock())){
					final = resp
				}
			}
		}
		success := count >= requirements[i]
		final.Success = proto.Bool(success)
		resp.Resps = append(resp.Resps, final)
	}
	
	return resp
		
}

func SendBytes(conn *net.TCPConn, data []byte) error {
	size := make([]byte, 4)
	binary.BigEndian.PutUint32(size, uint32(len(data)))
	n, err := conn.Write(size)
	if err != nil {
		return err
	}
	n, err = conn.Write(data)
	if err != nil {
		return err
	} else if n < len(data) {
		return errors.New("Lost Data")
	}

	return nil
}

func SendProto(conn *net.TCPConn, msg proto.Message) error {
	data, err := proto.Marshal(msg)
	if err != nil {
		return err
	}
	return SendBytes(conn, data)
}

func (s *KVServer) Execute(req *SingleRequest) (*SingleResponse) {
	op := req.GetOp()
	key := req.GetKey()
	value := req.GetValue()

	var result *kvstore.Value = nil
	var success = false

	switch {
	case op == SingleRequest_Write:
		clock := req.GetClock()
		success = s.store.Write(key, value, clock.ToVclock())
	case op == SingleRequest_Read:
		result, success = s.store.Read(key)
	}

	resp := &SingleResponse {
		Success: proto.Bool(success),
	}
	if result != nil {
		resp.Result = result.Data	
		resp.Clock = result.Clock.ToProto()
	}

	return resp
}

func (s *KVServer) append(host hostid.HostID) {
	s.lock.Lock()
	s.hosts = append(s.hosts, &host)
	sort.Sort(s.hosts)
	s.lock.Unlock()
}

func (s *KVServer) delete(host hostid.HostID) {
	s.lock.Lock()
	for i, curr := range s.hosts {
		if *curr == host {
			s.hosts = append(s.hosts[:i], s.hosts[i+1:]...)
			break
		}
	}
	sort.Sort(s.hosts)
	s.lock.Unlock()
}


//----------------------subscriber functions-----------------//
const (
	KVSERVER_ID uint32 = 1
)

func (s *KVServer) GetId() uint32 {
	return KVSERVER_ID
}

func (s *KVServer) GetData(host hostid.HostID) []byte {
	return nil
}

func (s *KVServer) OnUpdate(host hostid.HostID, data [] byte) {
	//TODO :: ?? nothing? I dunno
}

func (s *KVServer) OnJoin(host hostid.HostID) {
	s.append(host)
	s.redistribute()
}

func (s *KVServer) OnShutdown(host hostid.HostID) {
	s.delete(host)
	s.redistribute()
}

func (s *KVServer) redistribute() {
	s.lock.Lock()
	s.store.Lock.Lock()
	reqs := make(map[hostid.HostID] *MultiRequest)
	for key, value := range s.store.Data {
		replicas := s.GetReplicasAsync(key)
		for _, replica := range replicas {
			if replica == s.host {
				continue
			}
			log.Printf("KVServer: adding key %d to host %s:%d\n", key, net.IP(replica.IP), replica.Port)
			multi, exists := reqs[*replica] 
			if !exists {
				multi = &MultiRequest{
					Reqs: make([]*SingleRequest, 0),
				}
				reqs[*replica] = multi
			}
			req := &SingleRequest {
				Op:	SingleRequest_Write.Enum(),
				Key: proto.Uint32(key),
				Value: value.Data,
				Clock: value.Clock.ToProto(),
				Client: proto.Bool(false),
				Consistency: SingleRequest_One.Enum(),
			}
			multi.Reqs = append(multi.Reqs, req)
		}
	}
	s.store.Lock.Unlock()
	s.lock.Unlock()

	for host, req := range reqs {
		go s.MassSend(host, req)
	}

}

func (s *KVServer) MassSend(host hostid.HostID, multi *MultiRequest) {
	addr := host.ToTCPAddr()
	conn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		log.Printf("KVServer:  MassSend: failed to connect to %s:%d, error: %s\n", net.IP(host.IP), host.Port, err.Error())
	}
	err = SendProto(conn, multi)
	if err != nil {
		log.Printf("KVServer:  MassSend: failed to send to %s:%d, error: %s\n", net.IP(host.IP), host.Port, err.Error())
	}
	_, err = ReadAll(conn)
	if err != nil {
		log.Printf("KVServer:  MassSend: failed to read from %s:%d, error: %s\n", net.IP(host.IP), host.Port, err.Error())
	}

	conn.Close()	
}

func (req * SingleRequest) ToMulti() (*MultiRequest) {
	return &MultiRequest {
		Reqs: []*SingleRequest{req},
	}
}

func (multi *MultiResponse) First() (*SingleResponse) {
	return multi.GetResps()[0]
}

func (s *KVServer) OnStop() {

}
