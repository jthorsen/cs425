package movie

import (
    "code.google.com/p/goprotobuf/proto"
)

type Index map[string][]uint32

type Video struct {
	Title string
	Year string
	Episode string
	AirDate string
}	

type Pair struct {
	Word string
	Keys []uint32
}

func (v *Video) ToProto() *Video_Proto {
	return &Video_Proto {
		Title: proto.String(v.Title),
		Year: proto.String(v.Year),
		Episode: proto.String(v.Episode),
		AirDate: proto.String(v.AirDate),
	}
}

func (p *Pair) ToProto() *Index_Proto_Pair {
	return &Index_Proto_Pair {
		Word: proto.String(p.Word),
		Keys: p.Keys,
	}
}

func (m *Index) ToProto() *Index_Proto {
	indexProto := &Index_Proto {
		Pairs: make([]*Index_Proto_Pair, 0, len(*m)),
	}
	for word, keys := range *m {
		pair := &Index_Proto_Pair {
			Word: proto.String(word),
			Keys: keys,
		}		
		indexProto.Pairs = append(indexProto.Pairs, pair)
	}
	return indexProto
}

func (v *Video_Proto) ToVideo() *Video {
	return &Video {
		Title: v.GetTitle(),
		Year: v.GetYear(),
		Episode: v.GetEpisode(),
		AirDate: v.GetAirDate(),
	}
}

func (p *Index_Proto_Pair) ToPair() *Pair {
	return &Pair {
		Word: p.GetWord(),
		Keys: p.GetKeys(),
	}
}

func (m *Index_Proto) ToIndex() Index {
	index := make(Index)
	pairs := m.GetPairs()
	for _, pair := range pairs {
		index[pair.GetWord()] = pair.GetKeys()
	}
	return index
}
