package main

import (
	"flag"
	"net"
	"cs425/mp4/kvserver"
    "code.google.com/p/goprotobuf/proto"
	"log"
    "fmt"
    "cs425/mp4/movie"
)

func reqResp(m *kvserver.MultiRequest, address string) (*kvserver.MultiResponse, error) {
	addr, err := net.ResolveTCPAddr("tcp", address)
	if err != nil {
        return nil, err
	}
	conn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		return nil, err
	}
    defer conn.Close()

	//send request
	err = kvserver.SendProto(conn, m)
	if err != nil {
		return nil, err
	}

	//read response
	data, err := kvserver.ReadAll(conn)
	if err != nil {
		return nil, err
	}	

	temp := &kvserver.MultiResponse{}
	err = proto.Unmarshal(data, temp)
    if err != nil {
        return nil, err
    }
    return temp, nil
}

func main() {
	flag_myaddr := flag.String("server", "localhost:8080", "server address and port")
	flag_consistency := flag.String("consistency", "one", "the consistency level of the request [one, quorum, all]")
	flag.Parse()

	//check consistency
	var consistency kvserver.SingleRequest_Consistency
	switch {
	case *flag_consistency == "one":
			consistency = kvserver.SingleRequest_One
	case *flag_consistency == "quorum":
			consistency = kvserver.SingleRequest_Quorum
	case *flag_consistency == "all":
			consistency = kvserver.SingleRequest_All
	default:
		log.Fatal("Please provide a consistency level [one, quorum, all]\n")
	}

    // Get the lookup table
    req := &kvserver.SingleRequest {
        Client: proto.Bool(true),
        Op: kvserver.SingleRequest_Read.Enum(),
        Key: proto.Uint32(0),
        Consistency: consistency.Enum(),
    }
    resp, err := reqResp(req.ToMulti(), *flag_myaddr)
    if err != nil || ! resp.First().GetSuccess() {
        log.Fatal("Failed to get index: %s", err.Error())
    }
    tmp := &movie.Index_Proto{}
    err = proto.Unmarshal(resp.First().GetResult(), tmp)
    if err != nil {
        log.Fatal("Failed to get index: %s", err.Error())
    }
    index := tmp.ToIndex()

    // Get The Keys
    keys := flag.Args()
    multi := &kvserver.MultiRequest {
        Reqs: make([]*kvserver.SingleRequest, 0, len(keys)),
    }
    done := make(map[uint32]bool)
    for _, key := range keys {
        for _, mov := range index[key] {
            _, exists := done[mov]
            if exists {
                continue
            }
            done[mov] = true
            multi.Reqs = append(multi.Reqs, &kvserver.SingleRequest {
                Client: proto.Bool(true),
                Op: kvserver.SingleRequest_Read.Enum(),
                Key: proto.Uint32(mov),
                Consistency: consistency.Enum(),
            })
        }
    }

    // Get The Resulting Movies
    resp, err = reqResp(multi, *flag_myaddr)
    if err != nil {
        log.Fatal("Failed to get: %s", err)
    }
    for _, mov := range resp.GetResps() {
        if !mov.GetSuccess() {
            log.Printf("Failed to get movie")
        }
        tmp2 := &movie.Video_Proto{}
        err = proto.Unmarshal(mov.GetResult(), tmp2)
        if err != nil {
            log.Printf("Failed to unmarshal movie")
        }
        movie := tmp2.ToVideo()
        fmt.Printf("%s\t\t%s\n", movie.Title, movie.Episode)
    }
}
