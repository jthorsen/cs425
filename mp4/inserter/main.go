package main

import (
	"flag"
	"net"
	"cs425/mp4/kvserver"
	"cs425/mp4/movie"
    "code.google.com/p/goprotobuf/proto"
	"log"
	"fmt"
	"bufio"
    "regexp"
	"os"
	"strings"
)


func main() {
	flag_myaddr := flag.String("myaddr", "localhost:8080", "listen address and port")
	flag_file := flag.String("file", "movies.txt", "file to load values from")
	flag_consistency := flag.String("consistency", "one", "the consistency level of the request [one, quorum, all]")
	flag.Parse()



	//check consistency
	var consistency kvserver.SingleRequest_Consistency
	switch {
	case *flag_consistency == "one":
			consistency = kvserver.SingleRequest_One
	case *flag_consistency == "quorum":
			consistency = kvserver.SingleRequest_Quorum
	case *flag_consistency == "all":
			consistency = kvserver.SingleRequest_All
	default:
		log.Fatal("Please provide a consistency level [one, quorum, all]\n")
	}

	multi := &kvserver.MultiRequest{
		Reqs: make([]*kvserver.SingleRequest, 0),
	}

	file, err := os.Open(*flag_file)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer file.Close()


    reg := regexp.MustCompile("\"?([^\"(\t]*)\"?[ \t]*\\(([^)]+)\\)[ \t]*({(.*)})?[ \t]*([0-9?]+(-[0-9?]+)?)")
	scanner := bufio.NewScanner(file)
	index := make(movie.Index)
	i := uint32(1)
	for scanner.Scan() {
		line := scanner.Text() //get line
		matches := reg.FindAllStringSubmatch(line, -1)
        if len(matches) < 1 {
            log.Printf("Parse Failed: %s\n", line)
            continue
        }

		vid := &movie.Video_Proto {
			Title: proto.String(matches[0][1]),
			Year: proto.String(matches[0][2]),
			Episode: proto.String(matches[0][4]),
			AirDate: proto.String(matches[0][5]),
		}
		bytes, err := proto.Marshal(vid)
		if err != nil {
			log.Fatal(err.Error())
		}

		temp := make(map[string] bool)


		//append list of keys for each word in title and episode
		words := strings.Fields(vid.GetTitle())
		for _, word := range words {
			temp[word] = true
		}
		words = strings.Fields(vid.GetEpisode())
		for _, word := range words {
			temp[word] = true
		}
		for word, _ := range temp {
			_, exists := index[word]
			if !exists {
				index[word] = make([]uint32, 0)
			}
			index[word] = append(index[word], i)
		}

		//create request
		req := &kvserver.SingleRequest {
			Client: proto.Bool(true),
			Op: kvserver.SingleRequest_Write.Enum(),
			Key: proto.Uint32(i), //line #
			Value: bytes, //value read from file
			Consistency: consistency.Enum(),
		}

		multi.Reqs = append(multi.Reqs, req)
		i++
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err.Error())
	}

	//marshal index into proto
	bytes, err := proto.Marshal(index.ToProto())
	req := &kvserver.SingleRequest {
		Client: proto.Bool(true),
		Op: kvserver.SingleRequest_Write.Enum(),
		Key: proto.Uint32(0),
		Value: bytes,
		Consistency: consistency.Enum(),
	}
	multi.Reqs = append(multi.Reqs, req)
		

	//create tcp connection
	addr, err := net.ResolveTCPAddr("tcp", *flag_myaddr)
	if err != nil {
		log.Fatal(err)
	}
	conn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		log.Fatal(err.Error())
	}	

	//send request
	err = kvserver.SendProto(conn, multi)
	if err != nil {
		log.Fatal(err.Error())
	}

	//read response
	data, err := kvserver.ReadAll(conn)
	if err != nil {
		log.Fatal(err.Error())
	}	
	
	temp := &kvserver.MultiResponse{}
	proto.Unmarshal(data, temp)
	resp := temp.First()
	value := resp.GetResult()
	fmt.Printf("Result: %s, Success: %t\n", value, resp.GetSuccess())

	conn.Close()

}	
