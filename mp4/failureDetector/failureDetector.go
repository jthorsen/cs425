package failureDetector

import (
	"cs425/mp4/hostid"
	"cs425/mp4/gossiper"
	"time"
    "log"
    "net"
    "sync"
)

type Hosts []*hostid.HostID

const (
	FAILURE_DETECTOR_ID uint32 = 2
)

type entry struct {
    time time.Time
    alive bool
}

type FailureDetector struct {
	interval time.Duration
	timeout time.Duration
	times map[hostid.HostID] *entry
	gossiper *gossiper.Gossiper
    lock sync.Mutex
}

func NewFailureDetector(_gossiper *gossiper.Gossiper, timeout time.Duration, interval time.Duration) *FailureDetector {
	return &FailureDetector {
		timeout: timeout,
		interval: interval,
		times: make(map[hostid.HostID] *entry),
		gossiper : _gossiper,
	}
}

func (fd * FailureDetector) Start() {
	go fd.detect()
}

func (fd * FailureDetector) detect() {
	for {
        fd.lock.Lock()
		for host, last := range fd.times {
			if(time.Since(last.time) > fd.timeout*2) {
                log.Printf("FDetector: REMOVED %s:%d\n", net.IP(host.IP), host.Port)
	            delete(fd.times, host)
				fd.gossiper.Remove(host)
			} else if(time.Since(last.time) > fd.timeout && last.alive) {
	            log.Printf("FDetector: DEAD %s:%d\n", net.IP(host.IP), host.Port)
	            last.alive = false
				fd.gossiper.SetDead(host)
			}
		}
        fd.lock.Unlock()
		time.Sleep(fd.interval)
	}
}

func (fd * FailureDetector) GetId() uint32 {
	return FAILURE_DETECTOR_ID
}

func (fd * FailureDetector) GetData(host hostid.HostID) []byte {
	return nil
}

func (fd * FailureDetector) OnUpdate(host hostid.HostID, data []byte) {
    fd.lock.Lock()
    ent, exists := fd.times[host]
    if ! exists {
        fd.times[host] = &entry {
            time: time.Now(),
            alive: true,
        }
    } else {
        ent.time = time.Now()
        if ! ent.alive {
	        log.Printf("FDetector: REJOIN %s:%d\n", net.IP(host.IP), host.Port)
            ent.alive = true
        }
    }
    fd.lock.Unlock()
}

func (fd * FailureDetector) OnJoin(host hostid.HostID) {
    fd.lock.Lock()
	fd.times[host] = &entry {
        time: time.Now(),
        alive: true,
    }
    fd.lock.Unlock()
}

func (fd * FailureDetector) OnShutdown(host hostid.HostID) {
    fd.lock.Lock()
	delete(fd.times, host)
    fd.lock.Unlock()
}

func (fd * FailureDetector) OnStop() {
}
