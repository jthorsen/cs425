package main

import (
	"flag"
	"net"
	"cs425/mp4/kvserver"
    "code.google.com/p/goprotobuf/proto"
	"log"
	"fmt"
)

func main() {
	flag_myaddr := flag.String("myaddr", "localhost:8080", "listen address and port")
	flag_op := flag.String("op", "", "operation to perform [lookup, insert, update, delete]")
	flag_key := flag.Int("key", -1, "the key to query")
	flag_value := flag.String("value", "", "the value to insert or update")
	flag_consistency := flag.String("consistency", "", "the consistency level of the request [one, quorum, all]")
	flag.Parse()

	//check arguments
	var op kvserver.SingleRequest_Op
    switch {
    case *flag_op == "lookup":
		op = kvserver.SingleRequest_Read
	case *flag_op == "insert":
		op = kvserver.SingleRequest_Write
		if *flag_value == "" {
			log.Fatal("Please provide a -value flag for inserts\n")
		}
	case *flag_op == "update":
		op = kvserver.SingleRequest_Write
		if *flag_value == "" {
			log.Fatal("Please provide a -value for updates\n")
		}
	case *flag_op == "delete":
		op = kvserver.SingleRequest_Write
	default:
		log.Fatal("Please provide an -op [lookup, insert, update, delete]\n")
	}

	//check consistency
	var consistency kvserver.SingleRequest_Consistency
	switch {
	case *flag_consistency == "one":
			consistency = kvserver.SingleRequest_One
	case *flag_consistency == "quorum":
			consistency = kvserver.SingleRequest_Quorum
	case *flag_consistency == "all":
			consistency = kvserver.SingleRequest_All
	default:
		log.Fatal("Please provide a consistency level [one, quorum, all]\n")
	}


	if *flag_key < 0  || *flag_key > 1000000 {
		log.Fatal("Please provide a -key [1, 1000000]\n")
	}

	//create request
	req := &kvserver.SingleRequest {
		Client: proto.Bool(true),
		Op: op.Enum(),
		Key: proto.Uint32(uint32(*flag_key)),
		Value: []byte(*flag_value),
		Consistency: consistency.Enum(),
	}

	//create tcp connection
	addr, err := net.ResolveTCPAddr("tcp", *flag_myaddr)
	if err != nil {
		log.Fatal(err)
	}
	conn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		log.Fatal(err.Error())
	}	

	//send request
	err = kvserver.SendProto(conn, req.ToMulti())
	if err != nil {
		log.Fatal(err.Error())
	}

	//read response
	data, err := kvserver.ReadAll(conn)
	if err != nil {
		log.Fatal(err.Error())
	}	

	temp := &kvserver.MultiResponse{}
	proto.Unmarshal(data, temp)
	resp := temp.First()
	value := resp.GetResult()
	fmt.Printf("Result: %s, Success: %t\n", value, resp.GetSuccess())

	conn.Close()

}	
