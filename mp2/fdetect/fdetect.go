package main

import (
    "bufio"
    "code.google.com/p/goprotobuf/proto"
    "errors"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net"
    "os"
    "os/signal"
    "regexp"
    "sync"
	"time"
)

var (
    timeout time.Duration
    droprate float64
    myaddr string
    inserted bool
    update_perms bool

    hostLock sync.Mutex
    allhosts map[string] *Member
    running map[string] *Member
    self *Member
    sock *net.UDPConn
)

type Member struct {
    host string
	addr *net.UDPAddr
	heartbeat uint64
	time time.Time
    closed bool
}

func Tracker() {
    log.Print("System: Attempting Insert")
    for _, host := range flag.Args() {
        addr, err := net.ResolveUDPAddr("udp", host)
        if err != nil {
            fmt.Fprintf(os.Stderr, "%s\n", err)
            os.Exit(1)
        }
        self.time = time.Now()
        SendMember(self, addr)
    }

    // Send updates with the given frequency
    var listid = 0
    update_perms = true
    var perms []int
    for {
        start := time.Now()
        self.heartbeat += 1
        hostLock.Lock()
        if len(running) > 0 {
            if update_perms || listid >= len(running) {
                perms = rand.Perm(len(running))
                listid = 0
            }
            var to *net.UDPAddr
            elem := perms[listid]
            listid += 1
            for _, member := range running {
                if elem == 0 {
                    to = member.addr
                    break
                }
                elem -= 1
            }
            self.time = time.Now()
            SendMember(self, to)
            for _, member := range running {
                if dead := time.Since(member.time); dead >= timeout {
                    log.Printf("Event: Failed " + member.host + ", dead for " + dead.String())
                    delete(running, member.host)
                    update_perms = true
                }
                SendMember(member, to)
            }
        }
        hostLock.Unlock()
        time.Sleep(timeout/time.Duration(len(running)+2) - time.Since(start))
    }
}

func SendMember(mem *Member, addr *net.UDPAddr) error {
    if rand.Float64() < droprate {
        return errors.New("Message Dropped")
    }
    dgram := &Datagram {
        Host: proto.String(mem.host),
        Heartbeat: proto.Uint64(mem.heartbeat),
        Closed: proto.Bool(mem.closed),
        Diffns: proto.Int64(time.Since(mem.time).Nanoseconds()),
    }
    data, err := proto.Marshal(dgram)
    if err != nil {
        return err
    }
    num, err := sock.WriteToUDP(data, addr)
    if err != nil {
        return err
    }
    if num != len(data) {
        return errors.New("UDP didn't send enough data")
    }
    return nil
}

func Responder() {
    data := make([]byte, 1024)
    for {
        // Wait for new messages
        count, _, err := sock.ReadFromUDP(data)
        if err != nil {
            log.Print("Receiver: " + err.Error())
            continue
        }

        // Parse the protobuf
        dgram := &Datagram{}
        err = proto.Unmarshal(data[:count], dgram)
        if err != nil {
            log.Print("Receiver: " + err.Error())
            continue
        }
        host := dgram.GetHost()
        //log.Printf("Receiver: Got update for %s hb %d\n", host, dgram.GetHeartbeat())

        // Update the host tables
        hostLock.Lock()
        member, present := allhosts[host]
        if ! present {
            hn := regexp.MustCompile(`(.*)\.\d+`).FindStringSubmatch(host)
            if len(hn) != 2 {
                log.Print("Receiver: Invalid result set for " + host)
                hostLock.Unlock()
                continue
            }
            addr, err := net.ResolveUDPAddr("udp", hn[1])
            if err != nil {
                log.Print("Receiver: Failed to parse host " + hn[1])
                hostLock.Unlock()
                continue
            }
            member = new(Member)
            member.host = host
            member.addr = addr
            member.heartbeat = 0
            member.time = time.Now()
            allhosts[host] = member
            running[host] = member
            update_perms = true;
            log.Print("Event: Join " + host)
        }
        if member.heartbeat < dgram.GetHeartbeat() {
            member.heartbeat = dgram.GetHeartbeat()
            member.time = time.Now().Add(-time.Duration(dgram.GetDiffns()))
            if _, pass := running[host]; ! pass {
                running[host] = member
                update_perms = true;
                log.Print("Event: Rejoin " + host)
            }
        }
        closed := dgram.GetClosed()
        if closed {
            delete(running, host)
            update_perms = true;
            log.Print("Event: Closing " + host)
        }
        member.closed = closed
        hostLock.Unlock()
        inserted = true
    }
}

func printList() {
    reader := bufio.NewReader(os.Stdin)
    for {
        _, _, _ = reader.ReadLine()
        hostLock.Lock()
        fmt.Fprintf(os.Stderr, "-------- Membership Table\n")
        fmt.Fprintf(os.Stderr, "Host\t\t\t\tHeartbeat\tStatus\n")
        for _, member := range allhosts {
            if member != self {
                fmt.Fprintf(os.Stderr, "%s\t%d\t\t", member.host, member.heartbeat)
                if _, run := running[member.host]; run {
                    fmt.Fprintf(os.Stderr, "Alive\n")
                } else if member.closed {
                    fmt.Fprintf(os.Stderr, "Shutdown\n")
                } else {
                    fmt.Fprintf(os.Stderr, "Failed\n")
                }
            }
        }
        fmt.Fprintf(os.Stderr, "-------- End Membership Table\n")
        hostLock.Unlock()
    }
}

func main() {
    flag.StringVar(&myaddr, "myaddr", "127.0.0.1:8080", "listen address and port")
    flag_timeout := flag.String("timeout", "4s", "Length of time before a node is declared failed")
    flag.Float64Var(&droprate, "droprate", 0.0, "The percentage of packets to drop where 0.0 means none and 1.0 means all")
    flag.Parse()

    // Validate Droprate
    if droprate > 1 || droprate < 0 {
        fmt.Fprintf(os.Stderr, "Droprate must be between 0 and 1\n")
        os.Exit(1)
    }

    // Create timeout durations
    var err error
    timeout, err = time.ParseDuration(*flag_timeout)
    if err != nil {
        fmt.Fprintf(os.Stderr, "%s\n", err)
        os.Exit(1)
    }
    if err != nil {
        fmt.Fprintf(os.Stderr, "%s\n", err)
        os.Exit(1)
    }
    allhosts = make(map[string] *Member)
    running = make(map[string] *Member)

    // Setup the socket for messaging
    listenaddr, err := net.ResolveUDPAddr("udp", myaddr)
    if err != nil {
        fmt.Fprintf(os.Stderr, "%s\n", err)
        os.Exit(1)
    }
    sock, err = net.ListenUDP("udp", listenaddr)
    if err != nil {
        fmt.Fprintf(os.Stderr, "%s\n", err)
        os.Exit(1)
    }
    self = new(Member)
    self.host = fmt.Sprintf("%s.%d", listenaddr.String(), time.Now().Unix())
	self.addr = listenaddr
	self.heartbeat = 0
    allhosts[self.host] = self

    // Run the daemons
    go Responder()
    go Tracker()
    go printList()

    // Wait for Stop Notifications
    done := make(chan os.Signal)
    signal.Notify(done)
    <-done
    log.Print("System: Entering shutdown")

    // Signal that we are closing
    self.closed = true
    hostLock.Lock()
    for _, member := range running {
        SendMember(self, member.addr)
    }
    hostLock.Unlock()

    // Cleanup
    sock.Close()
}
