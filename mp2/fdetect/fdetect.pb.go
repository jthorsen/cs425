// Code generated by protoc-gen-go.
// source: fdetect.proto
// DO NOT EDIT!

package main

import proto "code.google.com/p/goprotobuf/proto"
import json "encoding/json"
import math "math"

// Reference proto, json, and math imports to suppress error if they are not otherwise used.
var _ = proto.Marshal
var _ = &json.SyntaxError{}
var _ = math.Inf

type Datagram struct {
	Host             *string `protobuf:"bytes,1,req,name=host" json:"host,omitempty"`
	Closed           *bool   `protobuf:"varint,2,req,name=closed" json:"closed,omitempty"`
	Heartbeat        *uint64 `protobuf:"varint,3,req,name=heartbeat" json:"heartbeat,omitempty"`
	Diffns           *int64  `protobuf:"varint,4,req,name=diffns" json:"diffns,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *Datagram) Reset()         { *m = Datagram{} }
func (m *Datagram) String() string { return proto.CompactTextString(m) }
func (*Datagram) ProtoMessage()    {}

func (m *Datagram) GetHost() string {
	if m != nil && m.Host != nil {
		return *m.Host
	}
	return ""
}

func (m *Datagram) GetClosed() bool {
	if m != nil && m.Closed != nil {
		return *m.Closed
	}
	return false
}

func (m *Datagram) GetHeartbeat() uint64 {
	if m != nil && m.Heartbeat != nil {
		return *m.Heartbeat
	}
	return 0
}

func (m *Datagram) GetDiffns() int64 {
	if m != nil && m.Diffns != nil {
		return *m.Diffns
	}
	return 0
}

func init() {
}
