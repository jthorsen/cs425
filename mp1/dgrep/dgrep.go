package main

import (
	"log"
	"flag"
	"net"
	"io"
	"bufio"
	"fmt"
	"sync"
	"os"
	"runtime"
)

func die(err error) {
	log.Print(err)
	runtime.Goexit()
}

func process(wg * sync.WaitGroup, addr string, args string) {
	defer wg.Done()

	//connect
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		die(err)
	}

	send(conn, args)
	recv(conn)
}

func send(conn net.Conn, args string) {
	n, err := conn.Write(([]byte)(args))
	if err != nil {
		die(err)
	} else if n < len(args) {
		runtime.Goexit()
	}
}

func recv(conn net.Conn) {
	reader := bufio.NewReader(conn)
	for {
		str, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		} else if err != nil {
			die(err)
		}

		fmt.Print(str)
	}
}

func parse(file string) []string {

	in, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}

	var addr []string
	reader := bufio.NewReader(in)
	for {
		str, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		addr = append(addr, str[0:len(str)-1])
	}

	return addr
}


func main() {

	//set default flag values
	key := flag.String("key", "", "pattern for log key")
	value := flag.String("value", "", "pattern for log value")

	//parse command line flags
	flag.Parse()

	//prepare grep command
	var args string = "grep \".*" + *key + ".*:.*" + *value + ".*\" *.log\n"


//	addr := parse("dgrep.config")
	var addr []string = []string{"localhost:8080", "localhost:9090", "localhost:9191", "localhost:9292"}
		
	var wg sync.WaitGroup
	wg.Add(len(addr))

	//send command to all machines
	for i := range addr {
		go process(&wg, addr[i], args)
	}

	wg.Wait()
}
