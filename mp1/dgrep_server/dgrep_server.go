package main

import (
	"bufio"
	"os/exec"
	"log"
	"net"
	"flag"
	"runtime"
)

func die(conn net.Conn, err error) {
	log.Print(err)
	conn.Close()
	runtime.Goexit()
}

func handleRequest(conn net.Conn) {

	//read grep command from socket
	str, err := bufio.NewReader(conn).ReadString('\n')
	if err != nil {
		die(conn, err)
	}

	//execute grep and pipe output over socket
	cmd := exec.Command("sh", "-c", str)
	cmd.Stdout = conn
	cmd.Stderr = conn
	err2 := cmd.Run()
	if err2 != nil {
		die(conn, err)
	}

	//close connection
	conn.Close()
}

func main() {

	addr := flag.String("addr", "localhost:8080", "listen address and port")

	flag.Parse()

	l, err := net.Listen("tcp", *addr)
	if err != nil {
		log.Fatal(err)
	}

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Print(err)
		}
		go handleRequest(conn)
	}
}
