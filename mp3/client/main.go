package main

import (
	"flag"
	"net"
	"cs425/mp3/kvserver"
    "code.google.com/p/goprotobuf/proto"
	"log"
	"fmt"
)

func main() {
	flag_myaddr := flag.String("myaddr", "localhost:8080", "listen address and port")
	flag_op := flag.String("op", "", "operation to perform [lookup, insert, update, delete]")
	flag_key := flag.Int("key", -1, "the key to query")
	flag_value := flag.String("value", "", "the value to insert or update")
	flag.Parse()

	//check arguments
	var op kvserver.Request_Op
    switch {
    case *flag_op == "lookup":
		op = kvserver.Request_Lookup
	case *flag_op == "insert":
		op = kvserver.Request_Insert
		if *flag_value == "" {
			log.Fatal("Please provide a -value flag for inserts\n")
		}
	case *flag_op == "update":
		op = kvserver.Request_Update
		if *flag_value == "" {
			log.Fatal("Please provide a -value for updates\n")
		}
	case *flag_op == "delete":
		op = kvserver.Request_Delete
	default:
		log.Fatal("Please provide an -op [lookup, insert, update, delete]\n")
	}
	if *flag_key < 0  || *flag_key > 1000000 {
		log.Fatal("Please provide a -key [1, 1000000]\n")
	}

	//create request
	req := &kvserver.Request {
		Client: proto.Bool(true),
		Op: op.Enum(),
		Key: proto.Uint32(uint32(*flag_key)),
		Value: []byte(*flag_value),
	}

	//create tcp connection
	conn, err := net.Dial("tcp", *flag_myaddr)
	if err != nil {
		log.Fatal(err.Error())
	}	

	//send request
	data, err := proto.Marshal(req)	
	if err != nil {
		log.Fatal(err.Error())
	}
	count, err := conn.Write(data)
	if err != nil {
		log.Fatal(err.Error())
	}

	//read response
	recv := make([]byte, 1024)
	count, err = conn.Read(recv)
	if err != nil {
		log.Fatal(err.Error())
	}	

	resp := &kvserver.Response{}
	proto.Unmarshal(recv[:count], resp)
	value := resp.GetResult()
	if value != nil {
		fmt.Printf("Result: %s\n", value)
	} else {
		fmt.Printf("Result: %t\n", resp.GetSuccess())
	}

	conn.Close()

}	
