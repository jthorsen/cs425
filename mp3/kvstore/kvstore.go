package kvstore

import "log"

type KVStore struct {
	Data map[uint32] []byte
}

func NewKVStore() *KVStore {
	return &KVStore { Data: make(map[uint32] []byte) }
}

func (kv *KVStore) Insert(key uint32, value []byte) (bool) {
	_, exists := kv.Data[key];
	if !exists {
    	log.Printf("KVStore: INSERT %d, %s\n", key, value)
		kv.Data[key] = value;
		return true;
	}
	return false;
}

func (kv *KVStore) Update(key uint32, value []byte) (exists bool) {
	_, exists = kv.Data[key]
	if exists {
    	log.Printf("KVStore: UPDATE %d, %s\n", key, value)
		kv.Data[key] = value;
	}
    return
}

func (kv *KVStore) Delete(key uint32) (exists bool) {
    log.Printf("KVStore: DELETE %d\n", key)
	_, exists = kv.Data[key]
	if exists {
		delete(kv.Data, key)
	}
    return
}

func (kv *KVStore) Lookup(key uint32) (data []byte, present bool) {
    log.Printf("KVStore: LOOKUP %d\n", key)
	data, present = kv.Data[key]
    return
}
