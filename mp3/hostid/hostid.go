package hostid

import (
   "net"
   "code.google.com/p/goprotobuf/proto"
   "time"
)

type HostID struct {
    IP string
    Port uint16
    Time int64
}

func (data *HostID_Proto) ToHostID() *HostID {
    return &HostID {
        IP: *data.IP,
        Port: uint16(*data.Port),
        Time: *data.Time,
    }
}

func (data *HostID_Proto) ToTCPAddr() *net.TCPAddr {
    return &net.TCPAddr {
        IP: []byte(*data.IP),
        Port: int(*data.Port),
    }
}

func (data *HostID_Proto) ToUDPAddr() *net.UDPAddr {
    return &net.UDPAddr {
        IP: []byte(*data.IP),
        Port: int(*data.Port),
    }
}

func TCPAddrToHostID(data *net.TCPAddr) *HostID {
    return &HostID {
        IP: string(data.IP),
        Port: uint16(data.Port),
        Time: time.Now().Unix(),
    }
}

func UDPAddrToHostID(data *net.UDPAddr) *HostID {
    return &HostID {
        IP: string(data.IP),
        Port: uint16(data.Port),
        Time: time.Now().Unix(),
    }
}

func (data *HostID) ToProto() *HostID_Proto {
    return &HostID_Proto {
        IP: proto.String(string(data.IP)),
        Port: proto.Uint32(uint32(data.Port)),
        Time: proto.Int64(data.Time),
    }
}

func (data *HostID) ToTCPAddr() *net.TCPAddr {
    return &net.TCPAddr {
        IP: []byte(data.IP),
        Port: int(data.Port),
    }
}

func (data *HostID) ToUDPAddr() *net.UDPAddr {
    return &net.UDPAddr {
        IP: []byte(data.IP),
        Port: int(data.Port),
    }
}
