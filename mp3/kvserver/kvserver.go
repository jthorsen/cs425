package kvserver

import (
	"fmt"
	"sync"
	"net"
	"code.google.com/p/goprotobuf/proto"
	"cs425/mp3/kvstore"
	"cs425/mp3/hostid"
	"log"
	"math"
	"sort"
)

type Hosts []*hostid.HostID

func (h Hosts) Len() int {
	return len(h)
}

func (h Hosts) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h Hosts) Less(i, j int) bool {
	if h[i].IP == h[j].IP {
		if h[i].Port == h[j].Port {
			return h[i].Time < h[j].Time
		}
		return h[i].Port < h[j].Port
	}
	return h[i].IP < h[j].IP
}

type KVServer struct {
	host *hostid.HostID
	addr *net.TCPAddr
	sock *net.TCPListener

	clientLock, lock, rbLock sync.Mutex
	rbOutstand map[hostid.HostID] int
	hosts Hosts
	store *kvstore.KVStore
}

func (s *KVServer) GetHost(key uint32) *hostid.HostID {
	chunk := math.Ceil(float64(1000001.0)/float64(len(s.hosts)))
	return s.hosts[key / uint32(chunk)]
}

func (s *KVServer) Contains(key uint32) bool {
	return *s.GetHost(key) == *s.host
}

func NewKVServer(host *hostid.HostID) *KVServer {
	return &KVServer {
		host: host,
		store: kvstore.NewKVStore(),
		hosts: Hosts{host},
		rbOutstand: make(map[hostid.HostID] int),
	}
}

func (s *KVServer) Start() error {
	s.addr = s.host.ToTCPAddr()

	var err error
	s.sock, err = net.ListenTCP("tcp", s.addr)
	if err != nil {
		return err
	}
	go s.listen()
	return nil
}

func (s *KVServer) listen() {
	for {
		conn, err := s.sock.AcceptTCP()
		if err != nil {
			log.Fatal(err.Error())
		}
		go s.handleRequest(conn)
	}
}

func (s *KVServer) MakeRequest(req *Request, resp *Response) *Response {
	bytes, err := proto.Marshal(req)
	if err != nil {
		log.Fatal(err.Error())
	}
	for {
		host := resp.GetRedirect()
		if host != nil {
			addr := host.ToTCPAddr()
			conn, err := net.DialTCP("tcp", nil, addr)
			if err != nil {
				log.Fatal(err.Error())
			}
			s.SendBytes(conn, bytes)
			data := make([]byte, 1024)
			count, err := conn.Read(data)
			if err != nil {
				log.Fatal(err.Error())
			}
			err = proto.Unmarshal(data[:count], resp)
			if err != nil {
				log.Fatal(err.Error())
			}
			conn.Close()
		} else {
			return resp
		}
	}
}

func (s *KVServer) handleRequest(conn *net.TCPConn) {
	data := make([]byte, 1024)
	count, err := conn.Read(data)
	if err != nil {
		log.Fatal(err.Error())
	}

	req := &Request{}
	err = proto.Unmarshal(data[:count], req)
	if err != nil {
		log.Fatal(err.Error())
	}

	var resp *Response
	if (req.GetOp() < 10) {
		s.clientLock.Lock()
		s.lock.Lock()
		resp = s.handleClientRequest(req)
		s.lock.Unlock()
		s.clientLock.Unlock()
	} else {
		resp = s.handleRebalanceRequest(req)
	}
	s.SendProto(conn, resp)
}

func (s *KVServer) hasOutstanding() (ret bool) {
	ret = false
	out := "["
	for _, val := range s.rbOutstand {
		out = fmt.Sprintf("%s%d,", out, val)
		if val > 0 {
			ret = true
		}
	}
	return 
}

func (s *KVServer) handleRebalanceRequest(req *Request) *Response {
	resp := &Response{}
	switch {
	case req.GetOp() == Request_Rebalance_New_Hosts:
		s.rbLock.Lock()
		s.lock.Lock()
		for _, host := range s.hosts {
			resp.Hosts = append(resp.Hosts, host.ToProto())
		}
		s.lock.Unlock()
		s.rbLock.Unlock()
	case req.GetOp() == Request_Rebalance_Done:
		host := *req.GetRemote().ToHostID()
		s.rbLock.Lock()
		s.rbOutstand[host]--
		if ! s.hasOutstanding() && s.rbOutstand[host] == 0 {
			s.clientLock.Unlock()
			resp.Success = proto.Bool(true)
		} else {
			resp.Success = proto.Bool(false)
		}
		s.rbLock.Unlock()
	default:
		s.lock.Lock()
		if s.Contains(req.GetKey()) {
			result, success := s.Execute(req)
			resp.Result = result
			resp.Success = proto.Bool(success)
			s.lock.Unlock()
		} else {
			s.lock.Unlock() // ORDER IS IMPORTANT TO PREVENT DEADLOCK
			host := s.GetHost(req.GetKey())
			resp.Redirect = host.ToProto()
			resp = s.MakeRequest(req, resp)
		}
	}
	return resp
}

func (s *KVServer) handleClientRequest(req *Request) *Response {
	key := req.GetKey()
	var resp *Response
	if s.Contains(key) { //if i have the value, send it back
		result, success := s.Execute(req)
		resp = &Response {
			Result: result,
			Success: proto.Bool(success),
		}
	} else { //else create a redirect
		host := s.GetHost(key)
		resp = &Response {
			Redirect: host.ToProto(),
		}
		if req.GetClient() { //if the request if from a client
			req.Client = proto.Bool(false)
			resp = s.MakeRequest(req, resp) //retreive the result
		}
	}
	return resp
}

func (s *KVServer) SendBytes(conn *net.TCPConn, data []byte) {
	n, err := conn.Write(data)
	if err != nil {
		log.Fatal(err.Error())
	} else if n < len(data) {
		log.Fatal("Lost data")
	}
}

func (s *KVServer) SendProto(conn *net.TCPConn, msg proto.Message) {
	data, err := proto.Marshal(msg)
	if err != nil {
		log.Fatal(err.Error())
	}
	s.SendBytes(conn, data)
}

func (s *KVServer) Execute(req *Request) ([]byte, bool) {
	op := req.GetOp()
	key := req.GetKey()
	value := req.GetValue()

	switch {
	case op == Request_Insert || op == Request_Rebalance_Insert:
		return nil, s.store.Insert(key, value)
	case op == Request_Update:
		return nil, s.store.Update(key, value)
	case op == Request_Delete:
		return nil, s.store.Delete(key)
	case op == Request_Lookup:
		return s.store.Lookup(key)
	}
	return nil, false
}

func (s *KVServer) append(host *hostid.HostID) {
	s.hosts = append(s.hosts, host)
	sort.Sort(s.hosts)
}

func (s *KVServer) delete(host *hostid.HostID) {
	for i, curr := range s.hosts {
		if *curr == *host {
			s.hosts = append(s.hosts[:i], s.hosts[i+1:]...)
			return
		}
	}
	sort.Sort(s.hosts)
}

func (s *KVServer) getIdx(h *hostid.HostID) int {
	for i, host := range s.hosts {
		if *h == *host {
			return i
		}
	}
	return -1
}

func (s *KVServer) redist(init []*hostid.HostID) {
	hosts := make(map[hostid.HostID] bool)
	if init != nil {
		for _, host := range init {
			if *host != *s.host {
				hosts[*host] = true
			}
		}
	}

	// Send all of the keys that need to be moved
	for key, value := range s.store.Data {
		if s.Contains(key) {
			continue
		}
		s.store.Delete(key)
		host := s.GetHost(key)
		hosts[*host] = true
		req := &Request{
			Op: Request_Rebalance_Insert.Enum(),
			Client: proto.Bool(true),
			Key: proto.Uint32(key),
			Value: value,
		}
		resp := &Response{
			Redirect: host.ToProto(),
		}
		s.MakeRequest(req, resp)
	}

	// Send all of the done messages
	for host, _ := range hosts {
		req := &Request{
			Op: Request_Rebalance_Done.Enum(),
			Remote: s.host.ToProto(),
		}
		resp := &Response{
			Redirect: host.ToProto(),
		}
		s.MakeRequest(req, resp)
	}
}

func (s *KVServer) popHostTables(host *hostid.HostID) {
	// Ask the connecting member
	req := &Request{
		Op: Request_Rebalance_New_Hosts.Enum(),
	}
	resp := &Response{
		Redirect: host.ToProto(),
	}
	resp = s.MakeRequest(req, resp)

	// Populate the host table
	for _, host := range resp.GetHosts() {
		if *host.ToHostID() != *s.host {
			s.hosts = append(s.hosts, host.ToHostID())
		}
	}
	sort.Sort(s.hosts)

	// Fill up our requirements
	myId := s.getIdx(s.host)
	if myId > 0 {
		s.rbOutstand[*s.hosts[myId-1]]++
	}
	if myId < len(s.hosts)-1 {
		s.rbOutstand[*s.hosts[myId+1]]++
	}
	s.redist(s.hosts)
}

func (s *KVServer) addMember(host *hostid.HostID) {
	// Check for duplicates
	for _, h := range s.hosts {
		if *h == *host {
			return
		}
	}

	s.append(host)

	// Determine our expected data provider
	myId := s.getIdx(s.host)
	newId := s.getIdx(host)
	providerId := newId
	supplierId := newId
	if (myId < newId) {
		providerId = myId - 1
		supplierId = myId + 1
	} else {
		providerId = myId + 1
		supplierId = myId - 1
	}
	if providerId >= 0 && providerId < len(s.hosts) {
		s.rbOutstand[*s.hosts[providerId]]++
	}
	s.rbOutstand[*host]++
	s.redist([]*hostid.HostID{s.hosts[supplierId]})
}

//----------------------subscriber functions-----------------//
const (
	KVSERVER_ID uint32 = 1
)

func (s *KVServer) GetId() uint32 {
	return KVSERVER_ID
}

func (s *KVServer) GetData(host *hostid.HostID) []byte {
	return nil
}

func (s *KVServer) OnUpdate(host *hostid.HostID, data [] byte) {
	//TODO :: ?? nothing? I dunno
}

func (s *KVServer) OnJoin(host *hostid.HostID) {
	// Hold proper locks
	s.rbLock.Lock()
	if ! s.hasOutstanding() {
		s.clientLock.Lock()
	}
	s.lock.Lock()

	if len(s.hosts) == 1 {
		// Bootstrap host tables
		s.popHostTables(host)
	} else {
		s.addMember(host)
	}
	
	// Possible Unlock client actions
	s.lock.Unlock()
	if ! s.hasOutstanding() {
		s.clientLock.Unlock()
	}
	s.rbLock.Unlock()
}

func (s *KVServer) OnShutdown(host *hostid.HostID) {
	// Hold proper locks
	s.rbLock.Lock()
	if ! s.hasOutstanding() {
		s.clientLock.Lock()
	}
	s.lock.Lock()

	myId := s.getIdx(s.host)
	leaveId := s.getIdx(host)
	providerId := leaveId
	supplierId := leaveId
	if (myId < leaveId) {
		providerId = myId + 1
		supplierId = myId - 1
	} else {
		providerId = myId - 1
		supplierId = myId + 1
	}
	if providerId >= 0 && providerId < len(s.hosts) {
		s.rbOutstand[*s.hosts[providerId]]++
	}
	var msg []*hostid.HostID = nil
	if supplierId >= 0 && supplierId < len(s.hosts) {
		msg = []*hostid.HostID{s.hosts[supplierId]}
	}
	s.delete(host)
	s.redist(msg)

	// Possible Unlock client actions
	s.lock.Unlock()
	if ! s.hasOutstanding() {
		s.clientLock.Unlock()
	}
	s.rbLock.Unlock()
}

func (s *KVServer) OnStop() {
	// Hold proper locks
	s.rbLock.Lock()
	if ! s.hasOutstanding() {
		s.clientLock.Lock()
	}
	s.lock.Lock()

 	// Redistribute keys on the ring
	myId := s.getIdx(s.host)
	var msg []*hostid.HostID = nil
	if (myId + 1 < len(s.hosts)) {
		msg = append(msg, s.hosts[myId+1])
	}
	if (myId - 1 >= 0) {
		msg = append(msg, s.hosts[myId-1])
	}
	s.delete(s.host)
	if len(s.hosts) > 0 {
		s.redist(msg)
	}

	// Possible Unlock client actions
	s.lock.Unlock()
	if ! s.hasOutstanding() {
		s.clientLock.Unlock()
	}
	s.rbLock.Unlock()

	s.sock.Close()
}
