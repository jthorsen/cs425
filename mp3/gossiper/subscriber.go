package gossiper

import (
	"cs425/mp3/hostid"
)

type Subscriber interface {
	GetId() uint32
	GetData(host *hostid.HostID) []byte
	OnUpdate(host *hostid.HostID, data [] byte) 
	OnJoin(host *hostid.HostID)
	OnShutdown(host *hostid.HostID)	
	OnStop()
}
