package gossiper

import (
    "code.google.com/p/goprotobuf/proto"
    "errors"
	"log"
	"net"
    "sync"
	"time"
	"cs425/mp3/hostid"
)

type Event int

const (
    Event_Inserted Event = 0
    Event_Timeout Event = 1
)

type member struct {
	host * hostid.HostID
	addr *net.UDPAddr
	heartbeat uint64
}

type Gossiper struct {
	sock *net.UDPConn
	self *member
	hosts map[hostid.HostID] * member 
	interval time.Duration
	nextMember int
	subscribers map[uint32] Subscriber
	initialized bool
	lock sync.Mutex
    event chan Event
}

func NewGossiper() *Gossiper {
	g := new(Gossiper)
	//g.sock = ? //initialized in init
	g.self = new(member)
	g.self.heartbeat = 0
	//g.self.host.name = ? //initialized in init
	//g.self.addr = ? //initialized in init
	//g.interval = ? //intervaled in init
	g.nextMember = 0
	g.hosts = make(map[hostid.HostID] *member)
	g.subscribers = make(map[uint32] Subscriber)
	return g
}

func (g *Gossiper) Subscribe(sub Subscriber) {
	g.subscribers[sub.GetId()] = sub
}

func (g *Gossiper) Init(host string, interval time.Duration) error {

 	// Setup the socket for messaging
	var err error
	g.self.addr, err = net.ResolveUDPAddr("udp", host)
	if err != nil {
		return err
	}

	g.sock, err = net.ListenUDP("udp", g.self.addr)
	if err != nil {
		return err
	}

	g.self.host = hostid.UDPAddrToHostID(g.self.addr)

	g.interval = interval
    g.event = make(chan Event, 10)

	return nil
}

func (g *Gossiper) Stop() {
    g.lock.Lock()
    g.self.heartbeat++
	for _, recvmem := range g.hosts {
		g.send(g.self, recvmem.addr, Datagram_Shutdown.Enum())
	}
	g.sock.Close()
    g.lock.Unlock()

	for _, sub := range g.subscribers {
		sub.OnStop()
	}
}

func (g *Gossiper) Start(hosts []*net.UDPAddr, insertTimeout time.Duration) error {
    g.lock.Lock()
	go g.recvGossip()
	go g.sendGossip()
    g.sendInserts(hosts)
    g.lock.Unlock()

    if len(hosts) > 0 {
        return g.waitForInsert(insertTimeout)
    }
    return nil
}

func (g *Gossiper) waitForInsert(timeout time.Duration) error {
    go func() {
        time.Sleep(timeout)
        g.event <- Event_Timeout
    }()
    for {
        ev := <- g.event
        if ev == Event_Inserted {
            break
        } else if ev == Event_Timeout {
            return errors.New("Insertion Timed Out")
        }
    }
    return nil
}

func (g *Gossiper) sendInserts(hosts []*net.UDPAddr) {
	for _, host := range hosts {
		g.send(g.self, host, Datagram_Insert.Enum())
	}
}

func (g *Gossiper) recvGossip() {

	data := make([]byte, 1024)
	for {
		//check for new message
		count, _, err := g.sock.ReadFromUDP(data)
        if err != nil {
            log.Print("Receiver: " + err.Error())
           	break
        }

		//unmarshal data
		dgram := &Datagram{}
		err = proto.Unmarshal(data[:count], dgram)
		if err != nil {
			log.Printf("Reader: failed to unmarshal data, %s", err.Error()) 
			continue
		}
	
		op := dgram.GetOp()
		host := dgram.GetHost().ToHostID()
 		g.lock.Lock();
		mem, present := g.hosts[*host]
		data := dgram.GetData()
	
		//if this is a new member, parse and create it
		if !present && op != Datagram_Shutdown {
			mem = new(member)
			mem.host = host
			mem.addr = host.ToUDPAddr()
			mem.heartbeat = dgram.GetHeartbeat()

            // Check to see if we are newly inserted
            if len(g.hosts) == 0 {
                g.event <- Event_Inserted
            }

			//insert new member
			g.hosts[*host] = mem	

			go g.onJoin(mem)

			if op == Datagram_Insert {
				for _, recvmem := range g.hosts {
					g.send(mem, recvmem.addr, Datagram_Gossip.Enum())
				}
			}
		}
		if mem.heartbeat < dgram.GetHeartbeat() {
			mem.heartbeat = dgram.GetHeartbeat()
			if op == Datagram_Shutdown {
				go g.onShutdown(mem)
			} else {
				go g.onUpdate(mem, data)
			}
		}

		g.lock.Unlock()
	}
}

func (g *Gossiper) sendGossip() {
	for {
		g.lock.Lock()
		g.self.heartbeat++;
		if len(g.hosts) > 0 {
			g.nextMember = (g.nextMember + 1) % len(g.hosts)
			var i=0
			var mem *member
			for _, mem = range g.hosts {
				if i == g.nextMember {
					break;
				}
				i++;
			}
			err := g.sendAll(mem.addr, Datagram_Gossip.Enum())
			if err != nil {
				log.Printf("Send: %s\n", err)
				g.lock.Unlock()
				return
			}
		}
		g.lock.Unlock()
		time.Sleep(g.interval)
	}
}

func (g *Gossiper) sendAll(to *net.UDPAddr, op *Datagram_Op) error {
	g.send(g.self, to, op)
	for _, mem := range g.hosts {
		err := g.send(mem, to, op)
		if err != nil {
			return err
		}
	}
	return nil
}

func (g *Gossiper) send(mem *member, to *net.UDPAddr, op *Datagram_Op) error {
	if string(mem.addr.IP) == string(to.IP) && mem.addr.Port == to.Port {
		return nil;
	}
	//convert member to protobuf
	dgram := &Datagram {
		Op: op,
		Host: mem.host.ToProto(),
		Heartbeat: proto.Uint64(mem.heartbeat),
	}

	for _, sub := range g.subscribers {
		subData := sub.GetData(mem.host)
		if subData == nil {
			continue
		}
		data := &Datagram_Data {
			Id: proto.Uint32(sub.GetId()),
			Data: subData,
		}
		dgram.Data = append(dgram.Data, data)
	}

	//marshal data
	data, err := proto.Marshal(dgram)
	if err != nil {
		return err
	}

	//send protobuf over sock
	num, err := g.sock.WriteToUDP(data, to)
	if err != nil {
		return err
	}
	if num != len(data) {
		return err
	}
	return nil
}

func (g *Gossiper) onJoin(mem *member) {
	for _, sub := range g.subscribers {
		sub.OnJoin(mem.host)
	}
}

func (g *Gossiper) onUpdate(mem *member, data_lists []*Datagram_Data) {
    for id, sub := range g.subscribers {
        var out []byte = nil
	    for _, data := range data_lists {
            if data.GetId() == id {
                out = data.GetData()
                break
            }
        }
        sub.OnUpdate(mem.host, out)
    }
}

func (g *Gossiper) onShutdown(mem *member) {
	for _, sub := range g.subscribers {
		sub.OnShutdown(mem.host)
	}
}
