package main

import(
	"flag"
	"time"
	"fmt"
	"os"
	"net"
	"log"
	"os/signal"
    "syscall"
	"cs425/mp3/gossiper"
	"cs425/mp3/kvserver"
	"cs425/mp3/hostid"
)

func main() {
	flag_myaddr := flag.String("myaddr", "localhost:8080", "listen address and port")
	flag_interval := flag.String("interval", "500ms", "gossip interval")
    flag.Parse()
	
    // Create interval durations
    interval, err := time.ParseDuration(*flag_interval)
    if err != nil {
        fmt.Fprintf(os.Stderr, "%s\n", err)
        os.Exit(1)
    }

    // Block signals until we have initialized
    done := make(chan os.Signal)
    signal.Notify(done, syscall.SIGINT)

	gossip := gossiper.NewGossiper()
	err = gossip.Init(*flag_myaddr, interval)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
        os.Exit(1)
	}

	var hosts []*net.UDPAddr
	for _, flag := range flag.Args() {
		addr, err := net.ResolveUDPAddr("udp", flag)
		if err != nil {
			log.Fatal(err)
		}
		hosts = append(hosts, addr)
	}
	
	addr, err := net.ResolveTCPAddr("tcp", *flag_myaddr)
	if err != nil {
		log.Fatal(err)
	}

	store := kvserver.NewKVServer(hostid.TCPAddrToHostID(addr))
	err = store.Start()
	if err != nil {
		log.Fatal(err)
	}
	gossip.Subscribe(store)
	gossip.Subscribe(gossiper.Subscriber(new(printer)))

    // TODO: Fix Hardcoded Timeout
	err = gossip.Start(hosts, time.Second * 2)
    if err != nil {
        log.Fatal(err)
    }

	// Wait for Stop Notifications
    <-done
    log.Print("System: Entering shutdown")
    gossip.Stop()
}
